package com.prathima.company.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.prathima.company.model.Company;

public interface CompanyRepository extends MongoRepository<Company, String>{
	Optional<List<Company>> findByRegistrationIdLike(String registrationId);
}
