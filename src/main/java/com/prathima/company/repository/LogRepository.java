package com.prathima.company.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.prathima.company.model.Log;

public interface LogRepository extends MongoRepository<Log, String>{

}
