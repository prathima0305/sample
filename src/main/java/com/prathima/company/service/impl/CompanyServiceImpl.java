package com.prathima.company.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prathima.company.model.Company;
import com.prathima.company.model.CompanyTransaction;
import com.prathima.company.model.SearchBO;
import com.prathima.company.repository.CompanyRepository;
import com.prathima.company.repository.CompanyTransactionRepository;
import com.prathima.company.service.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService {
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private CompanyTransactionRepository companyTransactionRepository;

	public List<Company> findAllCompanies() {
		List<Company> companiesList = companyRepository.findAll();
		System.out.println(companiesList.size());
		companiesList.stream().forEach(company -> System.out.println("companyName: " + company.getCompanyName()));
		return companiesList;
	}
	
	public List<Company> getCompanyDetails(String registrationId) {
		Optional<List<Company>> companyListOpt = companyRepository.findByRegistrationIdLike(registrationId);
		if(companyListOpt.isPresent()) {
			return companyListOpt.get();
		}
		return null;
	}
	
	public List<CompanyTransaction> getCompanyTransactions(SearchBO search) throws Exception{
		Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse(search.getStartDate());
		Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse(search.getEndDate());
		Optional<List<CompanyTransaction>> companyTransListOpt = companyTransactionRepository.findByRegistrationIdAndTransactionDateBetween(search.getRegistrationId(), startDate, endDate);
		if(companyTransListOpt.isPresent()) {
			return companyTransListOpt.get();
		}
		return null;
	}
	
	public String saveCompany(Company company){
		Company createdCompany = companyRepository.insert(company);
		return "Company has been created with id : "+createdCompany.getId();
	}
	
	public String insertCompanyTransaction(CompanyTransaction companyTrans){
		CompanyTransaction insertedTrans = companyTransactionRepository.insert(companyTrans);
		return "Trasaction has been created with id : "+insertedTrans.getId();
	}
}
