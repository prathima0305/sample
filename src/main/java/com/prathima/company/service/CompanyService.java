package com.prathima.company.service;

import java.util.List;

import com.prathima.company.model.Company;
import com.prathima.company.model.CompanyTransaction;
import com.prathima.company.model.SearchBO;

public interface CompanyService {
	public List<Company> findAllCompanies();
	public List<Company> getCompanyDetails(String registrationId);
	public List<CompanyTransaction> getCompanyTransactions(SearchBO search)throws Exception;
	public String saveCompany(Company company);
	public String insertCompanyTransaction(CompanyTransaction companyTrans);
}
